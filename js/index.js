function vehicle(propulsion){
	return {
		speed: 0,
		propulsion: propulsion,
		accelerate: function(){			
			this.speed += this.propulsion;			
		}
	}
}

function landVehicle(wheels){
	var propulsion = {
		radius: 5,
		units: wheels
	}
	this.acceleration = function(){
		return 2 * Math.PI * propulsion.radius; 
	}
	var that = vehicle(this.acceleration());	
	return that;
}

function airVehicle(noozle){
	var propulsion = {
		power: 5,
		afterBurner: "on"
	}
	this.acceleration = function(){
		return propulsion.afterBurner == "on" ? propulsion.power * 2 : propulsion.power;
	}
	var that = vehicle(this.acceleration());
	that.switchAfterBurners = function(afterBurner){
		propulsion.afterBurner = afterBurner;
	}
	return that;
}

function waterVehicle(propellers){
	var propulsion = {
		propellers: propellers,
		fins: 2,
		spin: "clockwise"
	}
	this.acceleration = function(){
		var acceleration = propulsion.fins;
		if (propulsion.spin == "counter-clockwise") { acceleration *= -1; }
		return acceleration;
	}
	var that = vehicle(this.acceleration());
	that.setPropellers = function(propellers){
		propulsion.propellers = propellers;
	}
	that.setNumberFins = function(number){
		propulsion.fins = number;
	}
	that.setSpinDirection = function(direction){
		propulsion.spin = direction;
	}
	that.getSpeed = function(){
		return that.speed;
	};
	return that;
}

function amphibiousVehicle(propellers, wheels){
	var mode = "water";	
	function setModeVehicle(){
		return mode == "water" ? new waterVehicle(propellers) : new landVehicle(wheels);
	}
	var that = setModeVehicle();
	that.switchMode = function(){
		mode = mode == "water" ? "land" : "water";
		that = {};
		that = setModeVehicle();
	}
	that.getMode = function(){
		return mode;
	}
	return that;
}